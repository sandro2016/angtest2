import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class TicketService {

    constructor(private http: HttpClient) {}

    public getTickets(): Observable<any> {
        const result = this.http.get('http://myflow.local/v2/organizations/12/tickets/open');
        // console.log(result);
        return result;
    }
}
