import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderComponent} from './components/header.component';
import {TicketContainerComponent} from './components/ticket-container.component';
import {TileViewComponent} from './components/tile-view.component';
import {TableViewComponent} from './components/table-view.component';
import {MatSlideToggleModule, MatToolbarModule} from '@angular/material';
import {TicketService} from './ticket.service';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {AuthInterceptor} from './auth.interceptor';

@NgModule({
    declarations: [HeaderComponent, TicketContainerComponent, TileViewComponent, TableViewComponent],
    imports: [
        CommonModule,
        MatSlideToggleModule,
        MatToolbarModule
    ],
    providers: [
        TicketService,
        [ { provide: HTTP_INTERCEPTORS, useClass:
            AuthInterceptor, multi: true } ]
    ],
    exports: [
        TicketContainerComponent
    ]
})

export class TicketModule {}
