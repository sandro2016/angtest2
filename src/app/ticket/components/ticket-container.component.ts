import {Component, OnInit} from '@angular/core';
import {TicketService} from '../ticket.service';

@Component({
    selector: 'app-ticket-container',
    templateUrl: './ticket-container.component.html',
    styleUrls: ['./ticket-container.component.css']
})

export class TicketContainerComponent implements OnInit {

    tableView: boolean;
    public tickets;
    constructor(private ticketService: TicketService) {}

    ngOnInit(): void {
        this.tableView = false;
        this.ticketService.getTickets().subscribe((tickets) => {
            this.tickets = tickets;
        });
    }

    getViewMode($event) {
        this.tableView = $event;
        console.log('parent: ' + this.tableView);
    }
}

