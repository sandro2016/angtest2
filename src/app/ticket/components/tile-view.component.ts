import {Component, Input, OnInit} from '@angular/core';
import {TicketService} from '../ticket.service';

@Component({
    selector: 'app-tile-view',
    templateUrl: './tile-view.component.html',
    styleUrls: ['./tile-view.component.css']
})

export class TileViewComponent {
    @Input() tickets;
}
