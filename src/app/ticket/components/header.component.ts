import {Component, EventEmitter, Output} from '@angular/core';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})

export class HeaderComponent {

    @Output() public pusher = new EventEmitter();

    changeView(e): void {
        this.pusher.emit(e.checked);
    }

}
